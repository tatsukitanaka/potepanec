require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:other_product) { create(:product) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario '商品の情報が正しく表示されていること' do
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  scenario 'taxonが紐付く場合、商品が属するカテゴリー一覧へ戻ること' do
    expect(page).to have_link "一覧ページへ戻る"
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario 'taxonが紐付かない場合、トップページへ戻ること' do
    visit potepan_product_path(other_product.id)
    expect(page).to have_link "一覧ページへ戻る"
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_path
  end

  scenario '関連商品が正しく表示され、リンクが正常に機能すること' do
    within ".productsContent" do
      expect(page).to have_selector ".productBox", count: 4
      expect(page).to have_link related_products[0].name
      expect(page).to have_link related_products[0].display_price
      click_on related_products[0].name
      expect(current_path).to eq potepan_product_path(related_products[0].id)
    end
  end
end
