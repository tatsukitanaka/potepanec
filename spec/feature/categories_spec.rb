require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxon) { create(:taxon, name: "Bags", taxonomy_id: 1, parent_id: taxonomy.root.id) }
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let!(:product) { create(:product, name: "RUBY TOTE", taxons: [taxon]) }
  let(:other_taxon) { create(:taxon, name: "Socks", taxonomy_id: 2) }
  let(:other_product) { create(:product, name: "RUBY SOCKS") }

  background do
    visit potepan_category_path(taxon.root.id)
  end

  scenario 'カテゴリーページが正しく表示されること' do
    expect(page).to have_content taxon.name
    expect(page).not_to have_content other_taxon.name
    expect(page).to have_content product.name
    expect(page).not_to have_content other_product.name
  end

  scenario 'カテゴリーサイドバーが表示され、リンクが正常に機能すること' do
    expect(page).to have_content "Categories"
    expect(page).to have_link "Bags"
    click_on "Bags"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario '商品名の商品詳細ページへのリンクが正常に機能すること' do
    expect(page).to have_link product.name
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
