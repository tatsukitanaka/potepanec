require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_product) { create(:product, taxons: [taxon]) }

  it '関連商品のみ表示されること' do
    expect(product.related_products).to match_array(related_product)
  end
end
