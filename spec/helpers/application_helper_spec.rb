require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'タイトルテスト' do
    include(ApplicationHelper)
    subject { full_title(page_title) }

    context 'page_titleが空の場合' do
      let(:page_title) { "" }

      it 'BIGBAG Storeのみ表示されること' do
        is_expected.to eq "BIGBAG Store"
      end
    end

    context 'page_tileにnilが渡された場合' do
      let(:page_title) { nil }

      it 'BIGBAG Storeのみ表示されること' do
        is_expected.to eq "BIGBAG Store"
      end
    end

    context 'page_titleに引数が渡された場合' do
      let(:page_title) { "RUBY ON RAILS TOTE" }

      it 'RUBY ON RAILS TOTE - BIGBAG Storeと表示されること' do
        is_expected.to eq "RUBY ON RAILS TOTE - BIGBAG Store"
      end
    end
  end
end
