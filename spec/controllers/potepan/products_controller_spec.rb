require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
    let!(:other_related_product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it 'リクエストに成功すること' do
      expect(response).to be_successful
    end

    it 'showテンプレートで表示されること' do
      expect(response).to render_template :show
    end

    it '@productが期待された値を持つこと' do
      expect(assigns(:product)).to eq(product)
    end

    it '関連商品が4つ表示されること' do
      expect(assigns(:related_products).count).to eq 4
    end

    it '関連した商品のみ表示されていること' do
      expect(assigns(:related_products)).not_to include other_related_product
    end

    it '関連商品に自分自身を含んでいないこと' do
      expect(assigns(:related_products)).not_to include product
    end
  end
end
