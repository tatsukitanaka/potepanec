require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe '#show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:params) { { id: taxon.id } }

    before do
      get :show, params: params
    end

    it 'リクエストに成功すること' do
      expect(response).to be_successful
    end

    it 'showテンプレートで表示されること' do
      expect(response).to render_template :show
    end

    it '@taxonが期待された値を持つこと' do
      expect(assigns(:taxon)).to eq(taxon)
    end

    it '@taxonomiesが期待された値を持つこと' do
      expect(assigns(:taxonomies).first).to eq(taxonomy)
    end

    it '@productが期待された値を持つこと' do
      expect(assigns(:products)).to eq(taxon.products)
    end
  end
end
